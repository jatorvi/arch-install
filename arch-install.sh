#!/bin/bash

set -euo pipefail

proxy() {
	PROXY="http://cache.lan:3142"
	export http_proxy=$PROXY
	export https_proxy=$PROXY
}

stage1(){
proxy
# set install destination instead of autodiscovery of first disk
DISK=""

if [ -z $DISK ]; then
	if [ -b /dev/nvme0n1 ]; then
		DISK="/dev/nvme0n1"
	elif [ -b /dev/vda ]; then
		DISK="/dev/vda"
	elif [ -b /dev/sda ]; then
		DISK="/dev/sda"
	else
		echo "no disk found, exiting"
		exit 0
	fi
fi

echo "using $DISK for install destination"

# find existing partitions on disk and delete them
mapfile -t parts < <(ls -r ${DISK}*)
for part in "${parts[@]}"; do
	wipefs -afq "$part"
done

echo "partitioning disk $DISK"
sgdisk -og $DISK
#create efi partition
sgdisk -n 1::+512M -t 1:ef00 $DISK
#create root partition
sgdisk -n 2:: -t 2:8300 $DISK

#create FSs
mkfs.fat -F32 ${DISK}1
mkfs.ext4 ${DISK}2

#mount
mount ${DISK}2 /mnt
mkdir /mnt/boot
mount ${DISK}1 /mnt/boot

#pacman mirror
echo "## Finland" > /etc/pacman.d/mirrorlist
echo "Server = http://arch.mirror.far.fi/\$repo/os/\$arch" > /etc/pacman.d/mirrorlist

#bootstrap
pacstrap /mnt base base-devel

#genfstab
genfstab -U /mnt >> /mnt/etc/fstab
#prepare stage2
cp "$(readlink -f "$0")" /mnt/root/
chmod +x "/mnt/root/$0"
#enter chroot
arch-chroot /mnt "/root/$0" stage2
#reboot after stage2
umount -R /mnt
reboot
}

stage2() {
proxy
ln -sf /usr/share/zoneinfo/Europe/Helsinki /etc/localtime
hwclock --systohc
#gen locales
sed -i 's|#fi_FI.UTF-8|fi_FI.UTF-8|' /etc/locale.gen
sed -i 's|#en_US.UTF-8|en_US.UTF-8|' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "KEYMAP=fi" > /etc/vconsole.conf
#set hostname
hostname="arch-$(uuidgen |awk -F- '{print $2}')"
echo "$hostname" > /etc/hostname
echo "127.0.1.1 ${hostname}.localdomain $hostname" >> /etc/hosts

mkinitcpio -p linux

#install and configure salt-minion
pacman --noconfirm -S salt
mkdir -p /etc/salt/minion.d
{
	echo "id: $hostname"
	echo "master: arch-salt"
} >/etc/salt/minion.d/minion.conf
systemctl enable salt-minion

#install bootloader
bootctl install
{
	echo "default arch"
	echo "timeout 3"
	echo "editor 1"
} >/boot/loader/loader.conf

rootpart=$(blkid "$(findmnt -fn / |awk '{print $2}')" |awk '{print $4}' |tr -d \")
{
	echo "title Arch Linux"
	echo "linux /vmlinuz-linux"
	echo "initrd /initramfs-linux.img"
	echo "options root=$rootpart rw net.ifnames=0"
} >/boot/loader/entries/arch.conf

systemctl enable dhcpcd@eth0

exit

}

#actually run something
$1
